package br.com.pnolli.identity.user.service;

import br.com.pnolli.identity.AbstractIntegrationTest;
import br.com.pnolli.identity.avro.UserCreatedEvent;
import br.com.pnolli.identity.user.events.consumer.UserCreatedListener;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.awaitility.Awaitility.await;

class UserServiceIntegrationTest extends AbstractIntegrationTest {

    @Autowired
    private UserCreatedListener listener;

    @Autowired
    private UserService subject;


    @Test
    void onceAUserHasBeenSuccessfullyCreatedThenSendEvent() {
        final var user = UserCreatedEvent.newBuilder()
                .setId(UUID.randomUUID().toString())
                .setName("test")
                .setEmail("test-mail@mail.com")
                .setCreationDate(Instant.now().toString())
                .setUpdatedDate(Instant.now().toString())
                .build();

        subject.send(user);


        await().atMost(5, TimeUnit.SECONDS).untilAsserted(() ->
                assertThat(listener.getUsers())
                        .first()
                        .usingRecursiveComparison()
                        .isEqualTo(user));
    }
}
