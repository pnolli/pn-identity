package br.com.pnolli.identity.containers;

import org.springframework.test.context.DynamicPropertyRegistry;
import org.testcontainers.containers.Network;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.redpanda.RedpandaContainer;

public class KafkaContainerIT {

    private static final Network network = Network.newNetwork();

    @Container
    public final static RedpandaContainer container = new RedpandaContainer("docker.redpanda.com/redpandadata/redpanda:v23.1.2")
            .withNetwork(network);



    public static void start() {
        container.start();
    }

    public static void stop() {
        container.stop();
    }

    public static void properties(final DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.kafka.bootstrap-servers", container::getBootstrapServers);
        dynamicPropertyRegistry.add("spring.kafka.properties.schema.registry.url", container::getSchemaRegistryAddress);
    }

    public static boolean isRunning() {
        return container.isRunning();
    }
}
