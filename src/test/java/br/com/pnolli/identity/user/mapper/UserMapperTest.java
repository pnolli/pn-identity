package br.com.pnolli.identity.user.mapper;

import br.com.pnolli.identity.avro.UserCreatedEvent;
import br.com.pnolli.identity.user.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThat;

class UserMapperTest {

    private UserMapper subject;

    @BeforeEach
    void setUp() {
        subject = new UserMapperImpl();
    }

    @Test
    void shouldConvertUserCreatedToUser() {
        final var uuid = UUID.randomUUID();
        final var timestamp = Instant.now();

        final var userCreated = UserCreatedEvent.newBuilder()
                .setId(uuid.toString())
                .setName("test-name")
                .setEmail("mail@mail.com")
                .setCreationDate(timestamp.toString())
                .setUpdatedDate(timestamp.toString())
                .build();

        final var expected = User.builder().id(uuid)
                .name("test-name")
                .email("mail@mail.com")
                .creationDate(timestamp)
                .updatedDate(timestamp)
                .build();


        assertThat(subject.from(userCreated))
                .usingRecursiveComparison()
                .isInstanceOf(User.class)
                .isEqualTo(expected);
    }
}
