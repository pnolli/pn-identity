package br.com.pnolli.identity.user.events.consumer;

import br.com.pnolli.identity.avro.UserCreatedEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaHandler;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@RequiredArgsConstructor
@KafkaListener(topics = "${spring.kafka.topics.user-created}", groupId = "${spring.kafka.consumer.group-id}")
public class UserCreatedListener {

    private final List<UserCreatedEvent> users = new ArrayList<>();

    @KafkaHandler
    public void handler(final UserCreatedEvent event) {
        users.add(event);
    }

    public List<UserCreatedEvent> getUsers() {
        return users;
    }
}
