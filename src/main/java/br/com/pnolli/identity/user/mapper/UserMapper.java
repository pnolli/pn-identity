package br.com.pnolli.identity.user.mapper;

import br.com.pnolli.identity.avro.UserCreatedEvent;
import br.com.pnolli.identity.user.model.User;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public abstract class UserMapper {

    public abstract User from(UserCreatedEvent event);
}
