package br.com.pnolli.identity.user.model;

import lombok.*;

import java.time.Instant;
import java.util.UUID;

@Builder(toBuilder = true)
@Getter
@ToString
@EqualsAndHashCode
@RequiredArgsConstructor
public class User {
    private final UUID id;
    private final String name;
    private final String email;
    private final Instant creationDate;
    private final Instant updatedDate;
}
