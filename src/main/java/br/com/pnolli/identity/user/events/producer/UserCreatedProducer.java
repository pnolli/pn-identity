package br.com.pnolli.identity.user.events.producer;

import br.com.pnolli.identity.avro.UserCreatedEvent;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

@Log4j2
@Component
@RequiredArgsConstructor
public class UserCreatedProducer {

    @Value("${spring.kafka.topics.user-created}")
    private String topic;
    private final KafkaTemplate<String, UserCreatedEvent> kafkaTemplate;

    public void send(final UserCreatedEvent event) {
        kafkaTemplate.send(topic, event);
    }
}
