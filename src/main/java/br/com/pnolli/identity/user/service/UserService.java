package br.com.pnolli.identity.user.service;

import br.com.pnolli.identity.avro.UserCreatedEvent;
import br.com.pnolli.identity.user.events.producer.UserCreatedProducer;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;


@Log4j2
@Service
@RequiredArgsConstructor
public class UserService {
    private final UserCreatedProducer producer;

    public void send(final UserCreatedEvent event) {
        producer.send(event);
    }
}
